var src = './src';
var dest = './build';

module.exports = {
  webpack: {
    entry: ['./src/js/index.js'],
    output: {
      path: __dirname + '/../build/js/',
      filename: 'app.js'
    },
    devtool: 'source-map',
    module: {
      loaders: [
        {
          test: /\.js$/,
          include: /src/,
          loader: 'babel-loader',
          query: {modules: 'common'}
          // query: {
          //   presets: ['es2015']
          // }
        },
        {
          test: /\.jade$/,
          loader: "jade-html" 
        },
        {
          test: /\.json$/,
          loader: 'json'
        },
        {
          test: /\.tag$/,
          loader: 'tag',
          query: {
            template: 'jade'
          }
        }
      ]
    }
  },

  watch: {
    src: src  + '/**/*'
  },

  jade: {
    src: [src + '/**/*.jade', '!' + src + '/**/_*.jade'],
    dest: dest
  },

  server: {
    src: dest,
    livereload: true,
    directoryListing: false,
    open: false,
    port: 9100,
    fallback: 'index.html'
  }
};

