var gulp = require('gulp');
var config = require('../config').webpack;
var webpack = require('webpack-stream');
var named = require('vinyl-named');
var uglify = require('gulp-uglify');
var size    = require('gulp-filesize');

gulp.task('uglifyJs', ['webpack'], function() {
  return gulp.src(config.output.path + config.output.filename)
    .pipe(uglify())
    .pipe(gulp.dest(config.output.path))
    .pipe(size());
});

gulp.task('webpack', function(callback) {
  return gulp.src(config.entry)
  .pipe(named())
  .pipe(webpack(config))
  .pipe(gulp.dest(config.output.path));
});

