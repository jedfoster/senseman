var gulp = require('gulp');
var config = require('../config');
var rename = require('gulp-rename');
var surge = require('gulp-surge')

gulp.task('deploy', ['build-production'], function () {
  gulp.src(config.server.src + '/index.html')
    .pipe(rename('200.html'))
    .pipe(gulp.dest(config.server.src));

  return surge({
    project: config.server.src,         // Path to your static build directory
    domain: 'miriamsenseman.unmarked.co'  // Your domain or Surge subdomain
  });
});

