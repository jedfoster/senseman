var gulp = require('gulp');
var config = require('../config');

gulp.task('build', ['webpack', 'jade'], function() {
  return gulp.src('./CNAME').pipe(gulp.dest(config.server.src));
});

gulp.task('build-production', ['uglifyJs', 'jade'], function() {
  return gulp.src('./CNAME').pipe(gulp.dest(config.server.src));
});

