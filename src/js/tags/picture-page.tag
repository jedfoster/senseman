picture-page
  nav
    a(href='{ previousItem }' if='{ previousItem }') ← Previous
    a(href='{ home }') Home
    a(href='{ nextItem }' if='{ nextItem }') Next →

  article
    header
      h1 Picture { id }
      p
        raw(content='{ description }')
    .image
      img(src='{ image(id) }')

  nav
    a(href='{ previousItem }' if='{ previousItem }') ← Previous
    a(href='{ home }') Home
    a(href='{ nextItem }' if='{ nextItem }') Next →

  script
    :coffee-script
      App = require '../App'

      @image = (id) ->
        "http://s3.drft.io.s3.amazonaws.com/miriam-senseman/catalog_images/large/#{ id }.jpg"

      @id = opts.id

      @description = App.items(opts.id, opts.showHidden).description.replace /\\n/g, '<br />'

      prev = App.previous @id, opts.showHidden
      next = App.next @id, opts.showHidden

      @home = "/#{ App.query opts }"
      @previousItem = prev && prev.id && "#{ prev.id }#{ App.query opts }"
      @nextItem = next && next.id && "#{ next.id }#{ App.query opts }"

      @on 'mount', App.setPageTitle.bind @

      riot.mount 'raw'

