home-page
  ul
    li(each='{ items }' class='{ hidden: hide }')
      a(href='/picture/{ id }{ query }')
        span { id }
        img(src='{ thumb(id) }')

  script
    :coffee-script
      App = require '../App'

      @thumb = (id) ->
        "http://s3.drft.io.s3.amazonaws.com/miriam-senseman/catalog_images/thumbs/#{ id }.jpg"

      @items = App.items null, opts.showHidden
      @query = App.query opts

      @on 'mount', App.setPageTitle

