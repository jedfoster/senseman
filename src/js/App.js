import {items as data} from './data.json';

var getItem;

getItem = function(id) {
  return data.filter(function (item) { return item.id == id; })[0]
}

module.exports = {
  items(id, showHidden = false) {
    var item;

    if (id) {
      item = getItem(id);
      if (!item.hide || showHidden) return item;
      return false
    }

    return data.filter(function (item) { return (showHidden ? true : !item.hide); });
  },

  previous(currentId, showHidden) {
    var item;

    item = data[data.indexOf(getItem(currentId)) - 1];

    if (item && item.hide && !showHidden) return this.previous(item.id, showHidden);

    return item;
  },

  next(currentId, showHidden) {
    var item;

    item = data[data.indexOf(getItem(currentId)) + 1];

    if (item && item.hide && !showHidden) return this.next(item.id, showHidden);

    return item;
  },

  query(opts) {
    return opts.showHidden ? '?hidden=1' : '';
  },

  setPageTitle() {
    var base = 'Miriam Senseman'

    if (this.id) {
      document.title = `${base} | Painting ${this.id}`;
    }
    else {
      document.title = `${base}'s Paintings`;
    }
  }
};

