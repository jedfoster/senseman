'use strict';

import riot from 'riot';
import './tags/picture-page.tag';
import './tags/home-page.tag';

import App from './App';

riot.tag('raw', '', function(opts) {this.root.innerHTML = opts.content});

riot.route.base('/');

riot.route('/picture/*(\\?..)?', function(id) {
  riot.mount('main', 'picture-page', {id: id, showHidden: riot.route.query().hidden});
});

riot.route('/..', function() {
  riot.mount('main', 'home-page', {showHidden: riot.route.query().hidden});
});

riot.route.start(true);

window.riot = riot;

