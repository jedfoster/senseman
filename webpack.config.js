module.exports = {
  cache: true,
  entry: ['./src/js/index.js'],
  output: {
    path: __dirname + '/build/js/',
    filename: 'app.js'
  },
  devtool: 'source-map',
  module: {
    loaders: [
      {
        test: /\.js$/,
        include: /src/,
        loader: 'babel-loader',
        // query: {modules: 'common'}
        query: {
          presets: ['es2015']
        }
      },
      {
        test: /\.jade$/,
        loader: "jade-html" 
      },
      {
        test: /\.json$/,
        loader: 'json'
      }
    ]
  }
};
